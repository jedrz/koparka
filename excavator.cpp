#include "excavator.h"

#include <cmath>
#include "utils.h"
#include "include/glm/glm.hpp"
#include "include/glm/gtx/rotate_vector.hpp"
#include "include/glm/gtc/type_ptr.hpp"
#include "common.h"
#include "app.h"

Excavator::Excavator()
    : pos(glm::vec2(0.0, 0.0)) ,
    speedVec(glm::vec2(0.0, 0.0)), speed(0.0), maxSpeed(0.0),
    acceleration(0.0),
    rot(0.0), deltaRot(0.0),
    platformRot(0.0), deltaPlatformRot(0.0),
    armPRot(45.0), deltaArmPRot(0.0),
    armBRot(80.0), deltaArmBRot(0.0),
    bucketRot(0.0), deltaBucketRot(0.0),
    particleEffect(30000),
    leftParticle(false)
{
    tracksOffset[0] = tracksOffset[1] = 0.0;

    qobj = gluNewQuadric();
}

Excavator::~Excavator()
{
    gluDeleteQuadric(qobj);
}

void Excavator::init()
{
    gluQuadricNormals(qobj, GLU_SMOOTH);
    particleEffect.init();
    particleEffect.setEmitter(this);
}

void Excavator::display()
{
    computeBucketRot();
    computeArmBRot();
    computeArmPRot();
    computePlatformRot();
    computeRot();
    computeSpeed();
    computePosition();
    computeTracksOffset();

    glPushMatrix();
    particleEffect.display();
    glPopMatrix();

    glTranslated(pos[0], 0.0, pos[1]); // przesuniecie koparki
    glRotatef(rot, 0.0, 1.0, 0.0); // obrot koparki

    glScalef(0.1, 0.1, 0.1); // zmniejszenie rozmariu koparki

    glPushMatrix();
    podwozie();
    glPopMatrix();

    glTranslatef(0.0, 5.0, 0.0); // podniesienie reszty elementow nad podwozie

    glRotatef(platformRot, 0.0, 1.0, 0.0); // obrot kabiny

    glPushMatrix();
    glTranslatef(0.0, 2.0, 5.0 - 1.0);
    ramiona();
    glPopMatrix();

    glPushMatrix();
    kabina();
    glPopMatrix();
}

void Excavator::kabina()
{
    glPushMatrix();
    // podnies reszte elementow nad rame
    glTranslatef(0.0, 1.0, 0.0);

    // kabina
    glPushMatrix();

    glColor3f(0.6, 0.6, 0.0);

    // blacha przednia
    glPushMatrix();
    glTranslatef(1.7 + 1.0 + 0.3, 1.0, 5.0 - 0.05);
    displayCuboid(4.0 - 0.6, 2.0, 0.1);
    glPopMatrix();

    // blachy boczne
    glPushMatrix();
    for (int x = -1; x <= 1; x += 2) {
        glPushMatrix();
        glTranslatef(x * (2.0 - 0.05) + 3.0, 2.5, 2.2 + 0.3);
        displayCuboid(0.1, 5.0, 5.0 - 0.6);
        glPopMatrix();
    }
    glPopMatrix();

    // blacha tylna
    glPushMatrix();
    glTranslatef(1.7 + 0.3 + 1.0, 4.5, 0.05);
    displayCuboid(4.0 - 0.6, 9.0, 0.1);
    glPopMatrix();

    // dach
    glPushMatrix();
    glTranslatef(2.0 + 1.0, 9.0 + 0.5, 2.5);
    displayCuboid(4.0, 1.0, 5.0);
    glPopMatrix();

    // belki
    glPushMatrix();
    glTranslatef(3.0, 4.5, 2.5);
    for (int x = -1; x <= 1; x += 2) {
        for (int z = -1; z <= 1; z += 2) {
            glPushMatrix();
            glTranslatef(x * (2.0 - 0.15), 0.0, z * (2.5 - 0.15));
            displayCuboid(0.3, 9.0, 0.3);
            glPopMatrix();
        }
    }
    glPopMatrix();

    glPopMatrix();

    // prawa strona
    glPushMatrix();
    glColor3f(0.6, 0.6, 0.0);
    glTranslatef(-3.0, 2.0, 2.5);
    displayCuboid(4.0, 4.0, 5.0);
    glPopMatrix();

    // obrot pokrywy, do ktorej jest dolaczone ramie
    float pokrAngle = glm::degrees(std::atan(5.0 / 4.0));
    // miedzy kabina, a prawa strona
    // tu gdzie jest dolaczone ramie
    glPushMatrix();
    glColor3f(0.5, 0.5, 0.1);
    glTranslatef(0.0, 2.0, 2.5);
    // nachylenie blachy
    glRotatef(pokrAngle, -1.0, 0.0, 0.0);
    // rozciagniecie do szerokosci miedzy kabina a ... oraz
    // dlugosci blachy do odleglosci miedzy pokrywa, a rama
    glScalef(2.0, std::sqrt(4.0 * 4.0 + 5.0 * 5.0), 1.0);
    // obrot do |-|
    glRotatef(45, 0.0, 0.0, 1.0);
    // kwadrat o boku 1
    gluDisk(qobj, 0.0, 0.5 * std::sqrt(2), 4, 2);
    glPopMatrix();

    // laczenie z ramieniem
    glPushMatrix();
    // na oko
    glTranslatef(0.0, 2.0 - 0.5, 5.0 - 1.25);
    glRotatef(pokrAngle, 1.0, 0.0, 0.0);
    zakonczenieWewnatrz();
    glPopMatrix();

    // pokrywa silnika
    glPushMatrix();
    glColor3f(0.6, 0.6, 0.0);
    glTranslatef(0.0, 2.0, -2.5);
    displayCuboid(10.0, 4.0, 5.0);
    glPopMatrix();

    glPopMatrix();

    // rama
    glPushMatrix();
    glColor3f(0.1, 0.1, 0.1);
    glTranslatef(0.0, 0.5, 0.0);
    displayCuboid(10.0, 1.0, 10.0);
    glPopMatrix();

    // szyby
    // przezroczyste elementy musza byc renderowane na koncu
    glPushMatrix();
    glColor4f(0.9, 0.9, 0.9, 0.2);

    // szyba przednia
    glPushMatrix();
    glTranslatef(1.7 + 1.0 + 0.3, 4.0 + 2.0 + 1.0, 5.0 - 0.05);
    displayCuboid(4.0 - 0.6, 8.0, 0.02);
    glPopMatrix();

    // szyby boczne
    for (int x = -1; x <= 1; x += 2) {
        glPushMatrix();
        glTranslatef(x * (2.0 - 0.05) + 3.0, 2.5 + 5.0 + 1.0, 2.2 + 0.3);
        displayCuboid(0.02, 5.0, 5.0 - 0.6);
        glPopMatrix();
    }

    glPopMatrix();
}

void Excavator::podwozie()
{
    glColor3f(0.1, 0.1, 0.1);

    // lacznik
    glPushMatrix();
    glTranslatef(0.0, 4.0, 0.0);
    glRotatef(90.0, -1.0, 0.0, 0.0);
    gluCylinder(qobj, 1.5, 1.5, 1.0, 8, 8);
    glPopMatrix();

    // podwozie
    glPushMatrix();
    glTranslatef(0.0, 1.5 + 1.0, 0.0);
    displayCuboid(8.0, 3.0, 3.0);
    glPopMatrix();

    for (int x = -1; x <= 1; x += 2) {
        // polaczenie z gasienicami
        glColor3f(0.2, 0.2, 0.2);
        glPushMatrix();
        glTranslatef(x * 4.5, 0.75 + 1.0, 0.0);
        displayCuboid(1.5, 1.5, 1.0);
        glPopMatrix();

        // gasienice
        glPushMatrix();
        glTranslatef(x * 5.0, 0.0, 0.0);
        gasienice((x == -1) ? tracksOffset[0] : tracksOffset[1], x == -1);
        glPopMatrix();
    }
}

void Excavator::gasienice(float offset, bool left)
{
    glTranslatef(0.0, 0.1, 0.0);

    const float segments = 5;

    // dol i gora gasienic
    const float height = 2 * segments / 3.14;
    for (int y = 0; y <= 1; ++y) {
        for (int i = -4; i < 4; ++i) {
            glPushMatrix();
            glTranslatef(0.0, y * height, i + ((y == 0) ? offset : (1 - offset)));
            lopatka(y == 0);
            glPopMatrix();
        }
    }

    // laczenie gory z dolem
    const float r = height / 2.0;
    for (int z = -1; z <= 1; z += 2) {
        glPushMatrix();
        glTranslatef(0.0, r, z * 4.0);
        for (int i = 0; i < segments; i++) {
            float angle = (i + offset) * 180.0 / segments;
            glPushMatrix();
            glTranslatef(0.0, -z * r * std::cos(glm::radians(angle)), z * r * std::sin(glm::radians(angle)));
            glRotatef(angle, -1.0, 0.0, 0.0);
            lopatka(z == 1);
            glPopMatrix();
        }
        glPopMatrix();
    }

    for (int z = -1; z <= 1; z += 2) {
        // kola
        glPushMatrix();
        glTranslatef(0.0, 1.6, z * 4.0);
        glRotatef(offset * 180.0 / segments, -1.0, 0.0, 0.0);
        kolo();
        glPopMatrix();

        // polaczenie z kolami
        glColor3f(0.4, 0.4, 0.4);
        glPushMatrix();
        glTranslatef((left) ? 0.5 : -0.5, 1.5, z * 3.0);
        displayCuboid(0.4, 0.5, 2.0);
        glPopMatrix();
    }

    // w srodku gasienic
    glPushMatrix();
    glTranslatef(0.0, 1.0 + 0.6, 0.0);
    displayCuboid(0.9, 2.0, 5.0);
    glPopMatrix();
}

void Excavator::lopatka(bool up)
{
    if (!up) {
        glRotatef(180, 1.0, 0.0, 0.0);
    }

    glColor3f(0.2f, 0.2f, 0.2f);

    // element do napedu
    glPushMatrix();
    glTranslatef(0.0, 0.3, 0.0);
    displayCuboid(0.7, 0.6, 0.5);
    glPopMatrix();

    // glowny element
    displayCuboid(2.0, 0.2, 0.8);

    // ciensze blachy
    glPushMatrix();
    glTranslatef(0.0, 0.0, 0.45);
    displayCuboid(2.0, 0.05, 0.1);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0.0, 0.0, -0.45);
    displayCuboid(2.0, 0.05, 0.1);
    glPopMatrix();
}

void Excavator::kolo()
{
    float r = 0.8;
    float h = 1.0;

    float rT = 0.4;
    float hT = 0.2;

    // tryby
    glColor3f(0.0, 0.0, 0.0);
    for (int i = 0; i < 10; ++i) {
        float angle = i * 360.0 / 10 + 18.0;
        glPushMatrix();
        glTranslatef(0.0, r * std::cos(glm::radians(angle)), r * std::sin(glm::radians(angle)));
        glRotatef(angle, 1.0, 0.0, 0.0);

        glRotatef(90.0, 0.0, 1.0, 0.0);
        for (int z = -1; z <= 1; z += 2) {
            glPushMatrix();
            glTranslatef(0.0, 0.0, z * hT / 2);
            if (z == -1) {
                // poprawienie normalnych
                glRotatef(180, 0.0, 1.0, 0.0);
            }
            gluDisk(qobj, 0.0, rT, 3, 1);
            glPopMatrix();
        }
        glTranslatef(0.0, 0.0, -hT / 2);
        gluCylinder(qobj, rT, rT, hT, 3, 1);
        glPopMatrix();
    }

    glColor3f(1.0, 0.0, 0.0);
    // dekle
    for (int x = -1; x <= 1; x += 2) {
        glPushMatrix();
        glTranslatef(x * h / 2, 0.0, 0.0);
        glRotatef(x * 90, 0.0, 1.0, 0.0);
        gluDisk(qobj, 0.0, r, 16, 1);
        glPopMatrix();
    }

    // kolo
    glPushMatrix();
    glRotatef(90, 0.0, 1.0, 0.0);
    glTranslatef(0.0, 0.0, -h / 2);
    gluCylinder(qobj, r, r, h, 16, 1);
    glPopMatrix();
}

// Rysuje ramiona z punktem zaczepienia (0,0,0).
void Excavator::ramiona()
{
    float armLength = 10.0 + 1.0 + 0.6 * (1 + std::sqrt(3) / 2.0);
    float armLengthRot = armLength - 1.0;
    float armHeight = 1.0;

    // podniesienie ramienia na os Z, Powrot + przesuniecie do 0,0,0
    glTranslatef(0.0, armHeight / 2.0, -2.5 + 2.5);
    glRotatef(armPRot, -1.0, 0.0, 0.0);

    glPushMatrix();
    glTranslatef(0.0, 0.0, armLengthRot / 2.0);
    ramie();
    glPopMatrix();

    glPushMatrix();
    // przesuniecie do konca pierwszego ramienia
    glTranslatef(0.0, 0.0, armLengthRot);
    glRotatef(-armBRot, -1.0, 0.0, 0.0);

    glPushMatrix();
    glTranslatef(0.0, 0.0, armLengthRot / 2.0);
    ramie();
    glPopMatrix();

    glPushMatrix();
    // przesuniecie do konca ramienia drugiego
    // powrot
    glTranslatef(0.0, 0.0, -0.5 + armLength);
    // obrot wokol poczatku lyzki
    glRotatef(bucketRot, -1.0, 0.0, 0.0);
    glTranslatef(0.0, 0.0, 0.8 * 3.0);
    lyzka();
    glPopMatrix();

    glPopMatrix();
}

void Excavator::zakonczenieWewnatrz()
{
    // zwrocenie /\ do przodu
    glRotatef(90, 1.0, 0.0, 0.0);
    // podstawy z boku
    glRotatef(90, 0.0, 1.0, 0.0);

    float r = 0.6;
    float h = 0.6;
    float sides = 3;

    glColor3f(1.0, 1.0, 0.2);

    // podstawy
    for (int z = -1; z <= 1; ++z) {
        glPushMatrix();
        glTranslatef(0.0, 0.0, z * h / 2);
        if (z == -1) {
            // poprawienie normalnych
            glRotatef(180, 0.0, 1.0, 0.0);
        }
        gluDisk(qobj, 0.0, r, sides, 1);
        glPopMatrix();
    }

    // graniastoslup
    glPushMatrix();
    glTranslatef(0.0, 0.0, -h / 2);
    gluCylinder(qobj, r, r, h, sides, 1);
    glPopMatrix();

    // laczenie
    float rJ = 0.1;
    float hJ = h * 1.9;

    glColor3f(0.8, 0.8, 0.4);
    glPushMatrix();
    glTranslatef(0.0, 0.0, -hJ / 2);
    gluCylinder(qobj, rJ, rJ, hJ, 10, 1);
    glPopMatrix();
}

void Excavator::zakonczenieZewnatrz()
{
    glColor3f(0.1, 0.1, 0.1);
    for (int x = -1; x <= 1; x += 2) {
        glPushMatrix();
        glTranslatef(x * 0.5, 0.0, 0.0);
        displayCuboid(0.2, 0.7, 1.0);
        glPopMatrix();
    }
}

void Excavator::ramie()
{
    float length = 10.0;

    glPushMatrix();
    glTranslatef(0.0, 0.0, (length + 0.6 * (std::sqrt(3) / 2)) / 2.0);
    zakonczenieWewnatrz();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0.0, 0.0, -length / 2 - 0.5);
    zakonczenieZewnatrz();
    glPopMatrix();

    glColor3f(1.0, 0.0, 0.0);
    displayCuboid(1.0, 1.0, length);
}

// Rysuje lyzke poziomo, na srodku, lekko przesunieta w dol.
void Excavator::lyzka()
{
    // zaczep
    glPushMatrix();
    // Przesuniecie na poczatek lyzki.
    glTranslatef(0.0, 0.0, -0.8 * 3.0 + -0.5);
    zakonczenieZewnatrz();
    glPopMatrix();

    glPushMatrix();
    // lyzka
    glColor3f(1.0, 1.0, 1.0);

    glScalef(1.0, 0.7, 0.8);
    glTranslatef(0.0, -1.0, 0.0);
    glRotatef(90, -1.0, 0.0, 0.0); // poziomo
    glTranslatef(0.0, -3.0, 0.0);

    const int size = 6;
    float zy[size][2] = {
        { 0.0, 6.0 },
        { 2.0, 6.0 },
        { 3.0, 5.5 },
        { 3.5, 4.5 },
        { 3.0, 3.5 },
        { 0.0, 0.0 }
    };

    // tyl
    for (int i = 0; i < size - 1; ++i) {
        glBegin(GL_QUADS);
        glm::vec3 a(-2.0, zy[i][1] - zy[i + 1][1], zy[i][0] - zy[i + 1][0]);
        glm::vec3 b(-2.0, 0.0, 0.0);
        glm::vec3 n = glm::normalize(glm::cross(a, b));
        glNormal3fv(glm::value_ptr(n));
        glVertex3f(-1.0, zy[i][1], zy[i][0]);
        glVertex3f(-1.0, zy[i + 1][1], zy[i + 1][0]);
        glVertex3f(1.0, zy[i + 1][1], zy[i + 1][0]);
        glVertex3f(1.0, zy[i][1], zy[i][0]);
        glEnd();
    }

    // boki
    for (int x = -1; x <= 1; x += 2) {
        glBegin(GL_POLYGON);
        glNormal3f(x, 0.0, 0.0);
        for (int i = 0; i < size; ++i) {
            glVertex3f(x, zy[i][1], zy[i][0]);
        }
        glEnd();
    }
    glPopMatrix();
}

void Excavator::emit(Particle& p)
{
    if (speed != 0.0) {
        if (randFloat() < 0.75) {
            emitSide(p);
        } else {
            emitBack(p);
        }
        p.color = glm::vec3(
            randFloat() * 0.5 + 0.1,
            randFloat() * 0.5 + 0.1,
            randFloat() * 0.4 + 0.1);
        leftParticle = !leftParticle;
    }
}

void Excavator::emitBack(Particle& p)
{
    float xOffset = (leftParticle) ? -0.45 : 0.45;
    float zPos = 0.5 * ((speed > 0.0) ? -1.0 : 1.0);
    p.position = glm::vec3(
        xOffset + randFloat() * 0.1 - 0.05,
        0.01 + randFloat() * 0.01,
        zPos + randFloat() * 0.1);
    // Obrot wektora wokol osi x, aby poprawic pozycje czastek, kiedy
    // koparka jest obrocona.
    p.position = glm::rotateY(p.position, rot);
    p.position += glm::vec3(pos[0], 0.0, pos[1]);
    p.velocity = glm::vec3(
        -speedVec[0] / 10.0 + (randFloat() * speed - speed / 2.0) / 2.0 ,
        (randFloat() * 0.01 + 0.001) * App::SCALE,
        -speedVec[1] / 10.0 + (randFloat() * speed - speed / 2.0) / 2.0);
    p.gravity = glm::vec3(0.0, -0.0001 * App::SCALE * App::SCALE, 0.0);
}

void Excavator::emitSide(Particle& p)
{
    float xOffset = (leftParticle) ? -0.5 : 0.5;
    float zPos = randFloat() * 1.0 - 0.5;
    p.position = glm::vec3(
        xOffset + randFloat() * 0.1 - 0.05,
        0.01 + randFloat() * 0.01,
        zPos);
    // Obrot wektora wokol osi x, aby poprawic pozycje czastek, kiedy
    // koparka jest obrocona.
    p.position = glm::rotateY(p.position, rot);
    p.position += glm::vec3(pos[0], 0.0, pos[1]);
    p.velocity = glm::vec3(
        -speedVec[0] / 2.0 + (randFloat() * speed - speed / 2.0) / 6.0,
        (randFloat() * 0.01 + 0.001) * App::SCALE / 1.2,
        -speedVec[1] / 2.0 + (randFloat() * speed - speed / 2.0) / 6.0);
    p.velocity = glm::rotateY(
        p.velocity,
        (50.0f + randFloat() * 30.0f) * (leftParticle ? 1 : -1) * (speed > 0 ? 1 : -1));
    p.gravity = glm::vec3(0.0, -0.0001 * App::SCALE * App::SCALE, 0.0);
}

void Excavator::go(float acc)
{
    acceleration = acc;
    maxSpeed = acc * 300;
    if (acc == 0.0) {
        speed = 0.0;
    }
}

void Excavator::rotate(float deltaRot)
{
    this->deltaRot = deltaRot;
}

void Excavator::rotatePlatform(float deltaPlatformRot)
{
    this->deltaPlatformRot = deltaPlatformRot;
}

void Excavator::rotateArmP(float deltaArmPRot)
{
    this->deltaArmPRot = deltaArmPRot;
}

void Excavator::rotateArmB(float deltaArmBRot)
{
    this->deltaArmBRot = deltaArmBRot;
}

void Excavator::rotateBucket(float deltaBucketRot)
{
    this->deltaBucketRot = deltaBucketRot;
}

void Excavator::computePosition()
{
    glm::vec2 newPos = pos + speedVec;
    if (App::terrain.getHeight(newPos[0], newPos[1]) <= 0.00001) {
        pos = newPos;
    }
}

void Excavator::computeSpeed()
{
    float newSpeed = speed + acceleration;
    if (newSpeed <= maxSpeed) {
        speed = newSpeed;
        speedVec = glm::vec2(
            std::sin(glm::radians(rot)) * speed,
            std::cos(glm::radians(rot)) * speed);
    }
}

void Excavator::computeRot()
{
    rot += deltaRot;
}

void Excavator::computePlatformRot()
{
    platformRot += deltaPlatformRot;
}

void Excavator::computeArmPRot()
{
    float newArmPRot = armPRot + deltaArmPRot;
    if (newArmPRot > 0 && newArmPRot < 90) {
        armPRot = newArmPRot;
    }
}

void Excavator::computeArmBRot()
{
    // ograniczenia
    float newArmBRot = armBRot + deltaArmBRot;
    if (newArmBRot >= 0 && newArmBRot < 120) {
        armBRot = newArmBRot;
    }
}

void Excavator::computeBucketRot()
{
    float newBucketRot = bucketRot + deltaBucketRot;
    if (newBucketRot > -90 && newBucketRot < 90) {
        bucketRot = newBucketRot;
    }
}

void Excavator::computeTracksOffset()
{
    float scaledSpeed = 3.0 * speed;
    float scaledRot = 0.1 * deltaRot;

    tracksOffset[0] += scaledSpeed + scaledRot;
    tracksOffset[1] += scaledSpeed - scaledRot;
    // sprawdzenie czy zakres [0, 1] nie zostal przekroczony
    for (int i = 0; i < 2; ++i) {
        while (tracksOffset[i] < 0.0) {
            tracksOffset[i] += 1.0;
        }
        while (tracksOffset[i] > 1.0) {
            tracksOffset[i] -= 1.0;
        }
    }
}