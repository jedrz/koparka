#ifndef _PARTICLE_EFFECT_H_
#define _PARTICLE_EFFECT_H_

#include <vector>

#include "include/glm/glm.hpp"

struct Particle
{
    glm::vec3 position;
    glm::vec3 velocity;
    glm::vec3 gravity;
    glm::vec3 color;
    Particle();
};

class Emitter
{
public:
    virtual void emit(Particle&) = 0;
};

class ParticleEffect
{
private:
    std::vector<Particle> particles;
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> colors;
    int textureId;
    const float PARTICLE_SIZE;
    Emitter* emitter;
    void adjustParticles();
    void buildVerticesBuffer();
    void buildColorsBuffer();
public:
    ParticleEffect(int);
    void init();
    void setEmitter(Emitter*);
    void display();
};

#endif