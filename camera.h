#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "include/glm/glm.hpp"

class Camera
{
public:
    glm::vec3 pos;
    glm::vec3 lookAtPos;
    float rotX, rotY;
    Camera();
    void display();
    void changeDistance(float);
    void rotateX(float);
    void rotateY(float);
    void moveX(float);
    void moveZ(float);
    void setFollowMode();
    void setFreeMode();
private:
    float distance, deltaDistance;
    float deltaRotX, deltaRotY;
    glm::vec3 deltaLookAtPos;
    bool followMode;
    void computeDistance();
    void computeRotX();
    void computeRotY();
    void computeLookAtPos();
    void computePos();
};

#endif