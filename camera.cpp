#include "camera.h"

#include <cmath>
#include "common.h"
#include "include/glm/glm.hpp"
#include "include/glm/gtx/rotate_vector.hpp"
#include "app.h"

Camera::Camera()
    : distance(10.0), deltaDistance(0.0),
    rotX(30.0), deltaRotX(0.0),
    rotY(180.0) /* 180, aby patrzec na tyl koparki */, deltaRotY(0.0),
    deltaLookAtPos(glm::vec3(0.0)),
    followMode(true)
{
    computeLookAtPos();
    computePos();
}

void Camera::display()
{
    computeDistance();
    computeRotX();
    computeRotY();
    computeLookAtPos();
    computePos();
    gluLookAt(
        pos.x, pos.y, pos.z,
        lookAtPos.x, lookAtPos.y, lookAtPos.z,
        0.0, 1.0, 0.0);
}

void Camera::changeDistance(float step)
{
    deltaDistance = step;
}

void Camera::rotateX(float delta)
{
    deltaRotX = delta;
}

void Camera::rotateY(float delta)
{
    deltaRotY = delta;
}

void Camera::moveX(float delta)
{
    deltaLookAtPos.x = delta;
}

void Camera::moveZ(float delta)
{
    deltaLookAtPos.z = delta;
}

void Camera::setFollowMode()
{
    followMode = true;
}

void Camera::setFreeMode()
{
    followMode = false;
}

void Camera::computeDistance()
{
    float newDistance = distance + deltaDistance;
    if (newDistance > 3 && newDistance < App::environment.MAX_Y) {
        distance = newDistance;
    }
}

void Camera::computeRotX()
{
    float newRotX = rotX + deltaRotX;
    if (newRotX > 5 && newRotX < 80) {
        rotX = newRotX;
    }
}

void Camera::computeRotY()
{
    rotY += deltaRotY;
}

void Camera::computeLookAtPos()
{
    if (followMode) {
        lookAtPos = glm::vec3(
            App::excavator.pos[0],
            1.0,
            App::excavator.pos[1]);
    } else {
        lookAtPos += glm::rotateY(deltaLookAtPos, rotY - 180);
    }
}

void Camera::computePos()
{
    pos = glm::vec3(
        distance * std::cos(glm::radians(rotX)) * std::sin(glm::radians(rotY)),
        distance * std::sin(glm::radians(rotX)) - 1.0, // ...
        distance * std::cos(glm::radians(rotX)) * std::cos(glm::radians(rotY)));
    pos += lookAtPos;
}