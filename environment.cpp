#include "environment.h"

#include "common.h"
#include "app.h"

Environment::Environment()
    : MIN_X(-200), MAX_X(200), MIN_Z(-200), MAX_Z(200), MAX_Y(70)
{
}

void Environment::init()
{
    texId = Textures::load("textures/skybox.jpg");
}

void Environment::display()
{
    displaySkyBox();
    displayFog();
}

void Environment::displaySkyBox()
{
    glBindTexture(GL_TEXTURE_2D, texId);

    // lewo
    glBegin(GL_QUADS);
    glTexCoord2f(0.0, 0.34);
    glVertex3f(MIN_X, 0.0f, MIN_Z);
    glTexCoord2f(0.0, 0.66);
    glVertex3f(MIN_X, 100.0f, MIN_Z);
    glTexCoord2f(0.25, 0.66);
    glVertex3f(MIN_X, 100.0f, MAX_Z);
    glTexCoord2f(0.25, 0.34);
    glVertex3f(MIN_X, 0.0f, MAX_Z);
    glEnd();

    // tyl
    glBegin(GL_QUADS);
    glTexCoord2f(0.25, 0.34);
    glVertex3f(MIN_X, 0.0f, MIN_Z);
    glTexCoord2f(0.25, 0.66);
    glVertex3f(MIN_X, MAX_Y,  MIN_Z);
    glTexCoord2f(0.5, 0.66);
    glVertex3f(MAX_X, MAX_Y,  MIN_Z);
    glTexCoord2f(0.5, 0.34);
    glVertex3f(MAX_X, 0.0f, MIN_Z);
    glEnd();

    // prawo
    glBegin(GL_QUADS);
    glTexCoord2f(0.5, 0.34);
    glVertex3f(MAX_X, 0.0f, MIN_Z);
    glTexCoord2f(0.5, 0.66);
    glVertex3f(MAX_X, 100.0f, MIN_Z);
    glTexCoord2f(0.75, 0.66);
    glVertex3f(MAX_X, 100.0f, MAX_Z);
    glTexCoord2f(0.75, 0.34);
    glVertex3f(MAX_X, 0.0f, MAX_Z);
    glEnd();

    // przod
    glBegin(GL_QUADS);
    glTexCoord2f(1.0, 0.34);
    glVertex3f(MIN_X, 0.0f, MAX_Z);
    glTexCoord2f(1.0, 0.66);
    glVertex3f(MIN_X, MAX_Y,  MAX_Z);
    glTexCoord2f(0.75, 0.66);
    glVertex3f(MAX_X, MAX_Y,  MAX_Z);
    glTexCoord2f(0.75, 0.34);
    glVertex3f(MAX_X, 0.0f, MAX_Z);
    glEnd();

    // gora
    glBegin(GL_QUADS);
    glTexCoord2f(0.25, 0.67);
    glVertex3f(MIN_X, MAX_Y, MIN_Z);
    glTexCoord2f(0.25, 1.0);
    glVertex3f(MIN_X, MAX_Y, MAX_Z);
    glTexCoord2f(0.5, 1.0);
    glVertex3f(MAX_X, MAX_Y, MAX_Z);
    glTexCoord2f(0.5, 0.67);
    glVertex3f(MAX_Z, MAX_Y, MIN_Z);
    glEnd();
}

void Environment::displayFog()
{
    float color[] = { 0.5, 0.5, 0.5, 1.0 };

    // rodzaj mgly
    glFogi(GL_FOG_MODE, GL_LINEAR);
    glFogfv(GL_FOG_COLOR, color);
    // opengl wybiera rodzaj mgly (na wierzcholek czy na piksel)
    glHint(GL_FOG_HINT, GL_DONT_CARE);
    // zakres mgly
    glFogf(GL_FOG_START, 2.0);
    glFogf(GL_FOG_END, 100);
}

void Environment::setFog(bool fog)
{
    if (fog) {
        glEnable(GL_FOG);
    } else {
        glDisable(GL_FOG);
    }
}