#include "textures.h"

#include "common.h"
#include "include/IL/il.h"
#include "include/IL/ilu.h"
#include "include/IL/ilut.h"

void Textures::init()
{
    ilInit();
    iluInit();
    ilutInit();
    ilutRenderer(ILUT_OPENGL);

    glEnable(GL_TEXTURE_2D);
}

int Textures::load(char* path)
{
    return ilutGLLoadImage((wchar_t*) path);
}