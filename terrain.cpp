#include "terrain.h"

#include "textures.h"
#include "app.h"
#include "include/glm/gtc/type_ptr.hpp"

Terrain::Terrain()
    : HEIGHT_MAP_SIZE(256), HM_STEP_SIZE(4)
    , heightMap(HEIGHT_MAP_SIZE, std::vector<float>(HEIGHT_MAP_SIZE, 0))
    , vertices(HEIGHT_MAP_SIZE * HEIGHT_MAP_SIZE)
    , texCoords(HEIGHT_MAP_SIZE * HEIGHT_MAP_SIZE)
    , normals(HEIGHT_MAP_SIZE * HEIGHT_MAP_SIZE)
{
}

void Terrain::init()
{
    texId = Textures::load("textures/ground.jpg");
    loadHeightMap();
    computeVertices();
    computeNormals();
    computeTexCoords();
    computeIndices();
}

void Terrain::loadHeightMap()
{
    FILE* fd = fopen("textures/ground_hm.bmp", "rb");
    fseek(fd, -3 * HEIGHT_MAP_SIZE * HEIGHT_MAP_SIZE, SEEK_END);
    for (int i = 0; i < HEIGHT_MAP_SIZE; ++i) {
        for (int j = 0; j < HEIGHT_MAP_SIZE; ++j) {
            heightMap[i][j] = (fgetc(fd) + fgetc(fd) + fgetc(fd)) / (3.0 * 256.0) * App::environment.MAX_Y;
        }
    }
    fclose(fd);
}

void Terrain::computeVertices()
{
    for (int x = 0; x < HEIGHT_MAP_SIZE; ++x) {
        for (int z = 0; z < HEIGHT_MAP_SIZE; ++z) {
            vertices[x * HEIGHT_MAP_SIZE + z] = glm::vec3(x, heightMap[x][z], z);
        }
    }
}

void Terrain::computeTexCoords()
{
    for (int x = 0; x < HEIGHT_MAP_SIZE; ++x) {
        for (int z = 0; z < HEIGHT_MAP_SIZE; ++z) {
            texCoords[x * HEIGHT_MAP_SIZE + z] = glm::vec2(
                (float) x / HEIGHT_MAP_SIZE * 3.0,
                (float) z / HEIGHT_MAP_SIZE * 3.0);
        }
    }
}

void Terrain::computeNormals()
{
    for (int x = 0; x < HEIGHT_MAP_SIZE; ++x) {
        for (int z = 0; z < HEIGHT_MAP_SIZE; ++z) {
            glm::vec3 sum;

            glm::vec3 out;
            if (z > 0) {
                out = glm::vec3(0.0f, heightMap[x][z - 1] - heightMap[x][z], -1.0f);
            }
            glm::vec3 in;
            if (z < HEIGHT_MAP_SIZE - 1) {
                in = glm::vec3(0.0f, heightMap[x][z + 1] - heightMap[x][z], 1.0f);
            }
            glm::vec3 left;
            if (x > 0) {
                left = glm::vec3(-1.0f, heightMap[x - 1][z] - heightMap[x][z], 0.0f);
            }
            glm::vec3 right;
            if (x < HEIGHT_MAP_SIZE - 1) {
                right = glm::vec3(1.0f, heightMap[x + 1][z] - heightMap[x][z], 0.0f);
            }

            if (x > 0 && z > 0) {
                sum += glm::normalize(glm::cross(out, left));
            }
            if (x > 0 && z < HEIGHT_MAP_SIZE - 1) {
                sum += glm::normalize(glm::cross(left, in));
            }
            if (x < HEIGHT_MAP_SIZE - 1 && z < HEIGHT_MAP_SIZE - 1) {
                sum += glm::normalize(glm::cross(in, right));
            }
            if (x < HEIGHT_MAP_SIZE - 1 && z > 0) {
                sum += glm::normalize(glm::cross(right, out));
            }

            sum = glm::normalize(sum);
            normals[x * HEIGHT_MAP_SIZE + z] = sum;
        }
    }
}

void Terrain::computeIndices()
{
    for (int x = 0; x < HEIGHT_MAP_SIZE - 1; ++x) {
        for (int z = 0; z < HEIGHT_MAP_SIZE - 1; ++z) {
            indices.push_back(x * HEIGHT_MAP_SIZE + z);
            indices.push_back((x + 1) * HEIGHT_MAP_SIZE + z);
            indices.push_back((x + 1) * HEIGHT_MAP_SIZE + z + 1);
            indices.push_back(x * HEIGHT_MAP_SIZE + z + 1);
        }
    }
}

void Terrain::display()
{
    glScalef(
        (float) (App::environment.MAX_X - App::environment.MIN_X) / HEIGHT_MAP_SIZE,
        1.0,
        (float) (App::environment.MAX_Z - App::environment.MIN_Z) / HEIGHT_MAP_SIZE);
    glTranslatef(-HEIGHT_MAP_SIZE / 2.0, 0.0, -HEIGHT_MAP_SIZE / 2.0);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glBindTexture(GL_TEXTURE_2D, texId);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glVertexPointer(3, GL_FLOAT, 0, &vertices[0]);
    glNormalPointer(GL_FLOAT, 0, &normals[0]);
    glTexCoordPointer(2, GL_FLOAT, 0, &texCoords[0]);

    glDrawElements(GL_QUADS, indices.size(), GL_UNSIGNED_INT, &indices[0]);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

float Terrain::getHeight(float x, float z)
{
    int xInd = (x + (App::environment.MAX_X - App::environment.MIN_X) / 2.0)
        * HEIGHT_MAP_SIZE / (App::environment.MAX_X - App::environment.MIN_X);
    int zInd = (z + (App::environment.MAX_Z - App::environment.MIN_Z) / 2.0)
        * HEIGHT_MAP_SIZE / (App::environment.MAX_Z - App::environment.MIN_Z);
    return heightMap[xInd][zInd];
}