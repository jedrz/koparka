#include "particleEffect.h"

#include "include/glm/gtc/type_ptr.hpp"

#include "common.h"
#include "textures.h"
#include "app.h"

Particle::Particle()
    : position(glm::vec3(0.0, 0.0, 0.0))
    , velocity(glm::vec3(0.0, 0.0, 0.0))
    , gravity(glm::vec3(0.0, 0.0, 0.0))
    , color(glm::vec3(0.0, 0.0, 0.0))
{
}

ParticleEffect::ParticleEffect(int num)
    : particles(std::vector<Particle>(num))
    , vertices(std::vector<glm::vec3>(num * 4))
    , colors(std::vector<glm::vec3>(num))
    , PARTICLE_SIZE(0.01)
{
}

void ParticleEffect::init()
{
    textureId = Textures::load("textures/particle.bmp");
}

void ParticleEffect::setEmitter(Emitter* emitter)
{
    this->emitter = emitter;
}

void ParticleEffect::buildVerticesBuffer()
{
    for (int i = 0; i < particles.size(); ++i) {
        const Particle& p = particles[i];
        float x = p.position.x;
        float y = p.position.y;
        float z = p.position.z;
        vertices[i * 4] =
            glm::vec3(x + PARTICLE_SIZE / 2, y + PARTICLE_SIZE / 2, z);
        vertices[i * 4 + 1] =
            glm::vec3(x - PARTICLE_SIZE / 2, y + PARTICLE_SIZE / 2, z);
        vertices[i * 4 + 2] =
            glm::vec3(x - PARTICLE_SIZE / 2, y - PARTICLE_SIZE / 2, z);
        vertices[i * 4 + 3] =
            glm::vec3(x + PARTICLE_SIZE / 2, y - PARTICLE_SIZE / 2, z);
    }
}

void ParticleEffect::buildColorsBuffer()
{
    for (int i = 0; i < particles.size(); ++i) {
        const Particle& p = particles[i];
        colors[i] = p.color;
    }
}

void ParticleEffect::adjustParticles()
{
    int emitted = 0;
    for (int i = 0; i < particles.size(); ++i) {
        Particle& p = particles[i];
        if (p.position.y <= 0.0 && emitted < particles.size() / 100) {
            emitter->emit(p);
            emitted++;
        }
        if (p.position.y > 0.0 - PARTICLE_SIZE) {
            p.velocity += p.gravity;
            p.position += p.velocity;
        }
    }
}

void ParticleEffect::display()
{
    adjustParticles();
    buildVerticesBuffer();
    buildColorsBuffer();

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, &vertices[0]);
    glColorPointer(3, GL_FLOAT, 0, &colors[0]);
    glDrawArrays(GL_QUADS, 0, vertices.size());
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
}