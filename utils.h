#ifndef _UTILS_H_
#define _UTILS_H_

#include "include/glm/glm.hpp"

void displayCuboid(float, float, float);

int randRange(int, int);
float randFloat();

struct Material
{
    glm::vec4 m_Ambient;
    glm::vec4 m_Diffuse;
    glm::vec4 m_Specular;
    glm::vec4 m_Emission;
    float  m_Shininess;

    Material( glm::vec4 ambient = glm::vec4(0.2, 0.2, 0.2, 1.0)
        , glm::vec4 diffuse = glm::vec4(0.8, 0.8, 0.8, 1.0)
        , glm::vec4 specular = glm::vec4(0.0, 0.0, 0.0, 1.0)
        , glm::vec4 emission = glm::vec4(0.0, 0.0, 0.0, 1.0)
        , float shininess = 0);

    void apply();
};

#endif