#ifndef _APP_H_
#define _APP_H_

#include "camera.h"
#include "excavator.h"
#include "environment.h"
#include "terrain.h"
#include "lighting.h"
#include "textures.h"

namespace App
{
    void init();
    void display();
    void reshape(int, int);
    void processNormalKey(unsigned char, int, int);
    void processNormalUpKey(unsigned char, int, int);
    void processSpecialKey(int, int, int);
    void processSpecialUpKey(int, int, int);

    extern Camera camera;
    extern Excavator excavator;
    extern Environment environment;
    extern Terrain terrain;
    extern Lighting lighting;
    extern float SCALE;
}

#endif