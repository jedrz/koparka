#ifndef _EXCAVATOR_H_
#define _EXCAVATOR_H_

#include "common.h"
#include "particleEffect.h"
#include "include/glm/glm.hpp"

class Excavator : public Emitter
{
public:
    glm::vec2 pos;
    Excavator();
    ~Excavator();
    void init();
    void display();
    void emit(Particle&);
    void go(float);
    void rotate(float);
    void rotatePlatform(float);
    void rotateArmP(float);
    void rotateArmB(float);
    void rotateBucket(float);
private:
    ParticleEffect particleEffect;
    GLUquadric* qobj;
    float speed, maxSpeed;
    glm::vec2 speedVec;
    float acceleration;
    float rot, deltaRot;
    float platformRot, deltaPlatformRot;
    float armPRot, deltaArmPRot;
    float armBRot, deltaArmBRot;
    float bucketRot, deltaBucketRot;
    float tracksOffset[2];
    bool leftParticle;
    void podwozie();
    void kabina();
    void gasienice(float, bool);
    void lopatka(bool);
    void kolo();
    void ramie();
    void zakonczenieWewnatrz();
    void zakonczenieZewnatrz();
    void lyzka();
    void ramiona();
    void emitSide(Particle&);
    void emitBack(Particle&);
    void computePosition();
    void computeSpeed();
    void computeRot();
    void computePlatformRot();
    void computeArmPRot();
    void computeArmBRot();
    void computeBucketRot();
    void computeTracksOffset();
};

#endif