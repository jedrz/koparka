#include "lighting.h"

#include "common.h"
#include "app.h"

Lighting::Lighting()
    : val(1.0)
{
}

void Lighting::init()
{
    glShadeModel(GL_SMOOTH);

    glEnable( GL_LIGHTING );
    glEnable( GL_LIGHT0 );

    glEnable(GL_NORMALIZE);

    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
}

void Lighting::display()
{
    float position[] = {
        App::environment.MAX_X,
        App::environment.MAX_Y,
        App::environment.MAX_Z
    };
    float ambient[] = { 0.6 * val, 0.6 * val, 0.6 * val, 1.0 };
    float diffuse[] = { 0.4 * val, 0.4 * val, 0.4 * val, 1.0 };

    glLightfv(GL_LIGHT0, GL_POSITION, position);
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
}

void Lighting::increase(float delta)
{
    float newVal = val + delta;
    if (newVal >= 0.0 && newVal <= 1.0) {
        val = newVal;
    }
}