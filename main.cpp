#include "common.h"
#include "app.h"

int main(int argc, char** argv)
{
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);

    glutInitWindowPosition(0, 0);
    glutInitWindowSize(800, 600);

    glutCreateWindow("Koparka");

    glutDisplayFunc(App::display);
    glutReshapeFunc(App::reshape);
    glutIdleFunc(App::display);

    glutKeyboardFunc(App::processNormalKey);
    glutKeyboardUpFunc(App::processNormalUpKey);

    glutSpecialFunc(App::processSpecialKey);
    glutSpecialUpFunc(App::processSpecialUpKey);

    glutIgnoreKeyRepeat(1);

    App::init();

    glutMainLoop();

    return 0;
}