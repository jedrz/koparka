#include "utils.h"

#include <cmath>
#include "common.h"

void displayCuboid(float xSize, float ySize, float zSize)
{
    glPushMatrix();
    glScalef(xSize, ySize, zSize);
    glutSolidCube(1.0);
    glPopMatrix();
}

int randRange(int min, int max)
{
    return rand() % (max - min) + min;
}

float randFloat()
{
    double max = RAND_MAX;
    return rand() / max;
}

Material::Material( glm::vec4 ambient
                   , glm::vec4 diffuse
                   , glm::vec4 specular
                   , glm::vec4 emission
                   , float shininess)
                   : m_Ambient( ambient )
                   , m_Diffuse( diffuse )
                   , m_Specular( specular )
                   , m_Emission( emission )
                   , m_Shininess( shininess )
{}

void Material::apply()
{
    glMaterialfv( GL_FRONT_AND_BACK, GL_AMBIENT, &(m_Ambient.r) );
    glMaterialfv( GL_FRONT_AND_BACK, GL_DIFFUSE, &(m_Diffuse.r) );
    glMaterialfv( GL_FRONT_AND_BACK, GL_SPECULAR, &(m_Specular.r) );
    glMaterialfv( GL_FRONT_AND_BACK, GL_EMISSION, &(m_Emission.r) );
    glMaterialf( GL_FRONT_AND_BACK, GL_SHININESS, m_Shininess );
}