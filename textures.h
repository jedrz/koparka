#ifndef _TEXTURES_H_
#define _TEXTURES_H_

namespace Textures
{
    void init();
    int load(char*);
}

#endif