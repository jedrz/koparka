#ifndef _LIGHTING_H_
#define _LIGHTING_H_

class Lighting
{
public:
    Lighting();
    void init();
    void display();
    void increase(float);
private:
    float val;
};

#endif