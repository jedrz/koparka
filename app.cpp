#include "app.h"

#include "common.h"

static const float EXC_SPEED = 0.01 / 100; // przyspieszenie
static const float EXC_ROT_STEP = 0.1;
static const float EXC_ROT_PLATFORM_STEP = 0.1;
static const float EXC_ROT_ARM_P_STEP = 0.1;
static const float EXC_ROT_ARM_B_STEP = 0.1;
static const float EXC_ROT_BUCKET_STEP = 0.1;

static const float CAMERA_DISTANCE_STEP = 0.04;
static const float CAMERA_ROT_STEP = 0.2;

static const float LIGHTING_DELTA = 0.1;

float App::SCALE = 2.3;
static const float DELTA_SCALE = 0.3;

Camera App::camera;
Excavator App::excavator;
Environment App::environment;
Terrain App::terrain;
Lighting App::lighting;

void App::init()
{
    // Wlacz z-bufor
    glEnable(GL_DEPTH_TEST);

    // przezroczystosc
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    lighting.init();
    Textures::init();
    environment.init();
    terrain.init();
    excavator.init();
}

void App::display()
{
    // Clear Color and Depth Buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Reset transformations
    glLoadIdentity();

    // Mnoz kolor tekstur z takim kolorem
    glColor3f(1.0, 1.0, 1.0);

    camera.display();

    lighting.display();

    glPushMatrix();
    environment.display();
    glPopMatrix();

    glPushMatrix();
    terrain.display();
    glPopMatrix();

    glPushMatrix();
    excavator.display();
    glPopMatrix();

    glFlush();
    glutSwapBuffers();
}

void App::reshape(int w, int h)
{
    if (w > 0 && h > 0) {
        float ratio = 1.0 * w / h;
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        // Set the viewport to be the entire window
        glViewport(0, 0, w, h);
        // Set the correct perspective.
        gluPerspective(45, ratio, 1, 1000);
        // Get Back to the Modelview
        glMatrixMode(GL_MODELVIEW);
    }
}

void App::processNormalKey(unsigned char key, int x, int y)
{
    switch (key) {
    case 'w':
        excavator.go(EXC_SPEED * SCALE);
        break;
    case 's':
        excavator.go(-EXC_SPEED * SCALE);
        break;

    case 'a':
        excavator.rotate(EXC_ROT_STEP * SCALE);
        break;
    case 'd':
        excavator.rotate(-EXC_ROT_STEP * SCALE);
        break;

    case 'h':
        excavator.rotatePlatform(EXC_ROT_PLATFORM_STEP * SCALE);
        break;
    case 'n':
        excavator.rotatePlatform(-EXC_ROT_PLATFORM_STEP * SCALE);
        break;

    case 'j':
        excavator.rotateArmP(EXC_ROT_ARM_P_STEP * SCALE);
        break;
    case 'm':
        excavator.rotateArmP(-EXC_ROT_ARM_P_STEP * SCALE);
        break;

    case 'k':
        excavator.rotateArmB(-EXC_ROT_ARM_B_STEP * SCALE);
        break;
    case ',':
        excavator.rotateArmB(EXC_ROT_ARM_B_STEP * SCALE);
        break;

    case 'l':
        excavator.rotateBucket(EXC_ROT_BUCKET_STEP * SCALE);
        break;
    case '.':
        excavator.rotateBucket(-EXC_ROT_BUCKET_STEP * SCALE);
        break;

    case '+':
        camera.changeDistance(CAMERA_DISTANCE_STEP * SCALE);
        break;
    case '-':
        camera.changeDistance(-CAMERA_DISTANCE_STEP * SCALE);
        break;

    case '8':
        camera.moveZ(CAMERA_DISTANCE_STEP * SCALE);
        break;
    case '2':
        camera.moveZ(-CAMERA_DISTANCE_STEP * SCALE);
        break;
    case '4':
        camera.moveX(CAMERA_DISTANCE_STEP * SCALE);
        break;
    case '6':
        camera.moveX(-CAMERA_DISTANCE_STEP * SCALE);
        break;

    case 'c':
        camera.setFollowMode();
        break;
    case 'C':
        camera.setFreeMode();
        break;

    case 'f':
        environment.setFog(true);
        break;
    case 'F':
        environment.setFog(false);
        break;

    case 'v':
        lighting.increase(LIGHTING_DELTA);
        break;
    case 'V':
        lighting.increase(-LIGHTING_DELTA);
        break;

    case '<':
        SCALE -= DELTA_SCALE;
        break;
    case '>':
        SCALE += DELTA_SCALE;
        break;

    case 27:
        exit(0);
        break;
    }
}

void App::processNormalUpKey(unsigned char key, int x, int y)
{
    switch (key) {
    case 'w':
    case 's':
        excavator.go(0.0);
        break;
    case 'a':
    case 'd':
        excavator.rotate(0.0);
        break;

    case 'h':
    case 'n':
        excavator.rotatePlatform(0.0);
        break;

    case 'j':
    case 'm':
        excavator.rotateArmP(0.0);
        break;

    case 'k':
    case ',':
        excavator.rotateArmB(0.0);
        break;

    case 'l':
    case '.':
        excavator.rotateBucket(0.0);
        break;

    case '+':
    case '-':
        camera.changeDistance(0.0);
        break;

    case '8':
    case '2':
        camera.moveZ(0.0);
        break;
    case '4':
    case '6':
        camera.moveX(0.0);
        break;
    }
}

void App::processSpecialKey(int key, int x, int y)
{
    switch (key) {
    case GLUT_KEY_UP:
        camera.rotateX(CAMERA_ROT_STEP * SCALE);
        break;
    case GLUT_KEY_DOWN:
        camera.rotateX(-CAMERA_ROT_STEP * SCALE);
        break;
    case GLUT_KEY_LEFT:
        camera.rotateY(CAMERA_ROT_STEP * SCALE);
        break;
    case GLUT_KEY_RIGHT:
        camera.rotateY(-CAMERA_ROT_STEP * SCALE);
        break;
    }
}

void App::processSpecialUpKey(int key, int x, int y)
{
    switch (key) {
    case GLUT_KEY_UP:
    case GLUT_KEY_DOWN:
        camera.rotateX(0.0);
        break;
    case GLUT_KEY_LEFT:
    case GLUT_KEY_RIGHT:
        camera.rotateY(0.0);
        break;
    }
}