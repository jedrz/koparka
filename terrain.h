#ifndef _TERRAIN_H_
#define _TERRAIN_H_

#include <vector>
#include "include/glm/glm.hpp"

class Terrain
{
public:
    const int HEIGHT_MAP_SIZE, HM_STEP_SIZE;
    Terrain();
    void init();
    void display();
    float getHeight(float, float);
private:
    std::vector<std::vector<float> > heightMap;
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texCoords;
    std::vector<int> indices;
    int texId;
    void loadHeightMap();
    void computeVertices();
    void computeNormals();
    void computeTexCoords();
    void computeIndices();
};

#endif