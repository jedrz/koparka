#ifndef _ENVIRONMENT_H_
#define _ENVIRONMENT_H_

#include <vector>

class Environment
{
public:
    const float MIN_X, MAX_X, MIN_Z, MAX_Z, MAX_Y;
    Environment();
    void init();
    void display();
    void setFog(bool);
private:
    int texId;
    void displaySkyBox();
    void displayFog();
};

#endif